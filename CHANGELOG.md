## [6.8.5] - 2024-10-10
### Library
- Packaged with `lib 6.8.59`

## [6.8.4] - 2024-08-28
### Fixed
- The `getDroppedFrames()` method to return the correct value.

## [6.8.3] - 2024-07-25
### Library
- Packaged with `lib 6.8.57`

## [6.8.2] - 2022-10-21
### Added
- Add support to detect the video playback speed (PlayRate)
- Report triggered events parameter for the fired plugin events
### Library
- Packaged with `lib 6.8.34`

## [6.8.1] - 2022-10-21
### Library
- Packaged with `lib 6.8.32`

## [6.8.0] - 2022-05-31
### Added
- Buffer detection with player events
- Optimized code for bitrate and rendition detection, removing duplicated code
### Removed
- Playhead monitor buffer detection
### Library
- Packaged with `lib 6.8.21`

## [6.7.2] - 2021-02-01
### Added
- Start detection for live videos starting with a seek
### Library
- Packaged with `lib 6.7.26`

## [6.7.1] - 2020-07-20
### Library
- Packaged with `lib 6.7.12`

## [6.7.0] - 2020-05-15
### Removed
- Cofde refator, removing GetPlayrate (causing issues) and unnecessary code playing with playhead monitor
### Fixed
- Error reporting codes and messages
### Library
- Packaged with `lib 6.7.6`

## [6.5.4] - 2019-08-30
### Fixed
- Fake buffer detection after resume and seek
- Unsafe access to getMetricsFor method
### Library
- Packaged with `lib 6.5.14`

## [6.5.3] - 2019-07-25
### Fixed
- Added checks to make access to player methods safe
### Library
- Packaged with `lib 6.5.8`

## [6.5.2] - 2019-07-03
### Library
- Packaged with `lib 6.5.6`

## [6.5.1] - 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.5.0] - 2019-05-31
### Library
- Packaged with `lib 6.5.1`

## [6.4.0] - 2018-10-04
### Library
- Packaged with `lib 6.4.8`

## [6.3.1] - 2018-07-30
### Library
- Packaged with `lib 6.3.4`
### Added
- GetPlayrate, getRendition, getDroppedFrames, getLatency methods.

## [6.3.0] - 2018-07-09
### Library
- Packaged with `lib 6.3.2`

## [6.2.0] - 2018-04-09
### Library
- Packaged with `lib 6.2.0`

## [6.1.0] - 2017-10-25
### Library
- Packaged with `lib 6.1.0`

## [6.0.0] - 2017-10-05
### Library
- Packaged with `lib 6.0.8`
