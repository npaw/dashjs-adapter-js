/* global dashjs */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.DashJS = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = null
    if (this.player) {
      ret = this.isLive ? this.player.timeAsUTC() : this.player.time()
    }
    return ret
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    var ret = 0
    if (this.player) {
      var metrics = this.player.getDashMetrics()
      ret = metrics.getCurrentDroppedFrames().droppedFrames
    }
    return ret
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = null
    if (this.player) {
      ret = this.player.duration()
    }
    return ret
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var rendition = this._getRenditionInfo()
    return rendition ? rendition.bitrate : null
  },

  /** Override to return rendition */
  getRendition: function () {
    var rendition = this._getRenditionInfo()
    return rendition ? youbora.Util.buildRenditionString(rendition.width, rendition.height, rendition.bitrate) : null
  },

  _getRenditionInfo: function () {
    var ret = null
    if (this.player) {
      var level = this.player.getQualityFor('video')
      if (level) {
        ret = this.player.getBitrateInfoListFor('video')[level]
      }
    }
    return ret
  },

  /** Override to return user bandwidth throughput */
  getThroughput: function () {
    if (this.getCdnTraffic() && this.getCdnTraffic() !== 0) {
      if (!this.lastDataValue) {
        this.lastDataValue = 0
      }
      var prevDataValue = this.lastDataValue
      this.lastDataValue = this.getCdnTraffic() + this.getP2PTraffic()
      return Math.round((this.lastDataValue - prevDataValue) / (this.plugin._ping.interval / 1000))
    }
    if (!this.throughput && this.player && this.player.getMetricsFor) {
      var metrics = this.player.getMetricsFor('video')
      var dashMetrics = this.player.getDashMetrics()
      var requests = dashMetrics.getHttpRequests(metrics)
      if (!requests) return null
      var bps = 0
      var bpsCount = 0
      for (var i = 0; i < requests.length; i++) {
        var req = requests[i]
        if (req.type === 'MediaSegment' && req.responsecode >= 200 && req.responsecode < 400 && req._stream === 'video') {
          var match = req._responseHeaders.match(/Content-Length: (.+)/i)
          if (match) {
            var time = (req.interval - (req.tresponse - req.trequest)) / 1000
            bps += (match[1] / time) * 8
            bpsCount++
          }
        }
      }
      if (!bps) return null
      this.throughput = bps / bpsCount
    }
    return this.throughput
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    if (!this.isLive) {
      this.isLive = false
    }
    return this.isLive
  },

  /** Override to return resource URL. */
  getResource: function () {
    var ret = null
    if (this.player) {
      var src = this.player.getSource()
      if (typeof src === 'string') {
        ret = src
      }
    }
    return ret
  },

  /** Return playrate value */
  getPlayrate: function() {
    if (this.player) {
      return this.player.getPlaybackRate()
    }
    return 1
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'dashJS'
  },

  getPlayerVersion: function () {
    var ret = null
    if (this.player) {
      ret = this.player.getVersion()
    }
    return ret
  },

  getLatency: function () {
    var ret = null
    if (this.getIsLive() && this.player && this.player.getCurrentLiveLatency) {
      ret = this.player.getCurrentLiveLatency() * 1000
    }
    return ret
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    var Events = dashjs.MediaPlayer.events

    // Register listeners
    this.references = {}
    this.references[Events.MANIFEST_LOADED] = this.manifestLoaded.bind(this)
    this.references[Events.PLAYBACK_STARTED] = this.playbackStarted.bind(this)
    // this.references[Events.PLAYBACK_RATE_CHANGED] = this.playbackRateChanged.bind(this)
    this.references[Events.PLAYBACK_WAITING] = this.playbackWaiting.bind(this)
    this.references[Events.PLAYBACK_STALLED] = this.playbackWaiting.bind(this)
    this.references[Events.PLAYBACK_PLAYING] = this.playbackPlaying.bind(this)
    this.references[Events.PLAYBACK_SEEKED] = this.playbackPlaying.bind(this)
    this.references[Events.PLAYBACK_PAUSED] = this.playbackPaused.bind(this)
    this.references[Events.PLAYBACK_ENDED] = this.playbackEnded.bind(this)
    this.references[Events.ERROR] = this.errorListener.bind(this)
    this.references[Events.PLAYBACK_ERROR] = this.playbackError.bind(this)
    this.references[Events.PLAYBACK_SEEKING] = this.playbackSeeking.bind(this)
    if (this.player) {
      for (var key in this.references) {
        this.player.on(key, this.references[key])
      }
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      delete this.references
    }
  },

  /** Listener for live. */
  manifestLoaded: function (e) {
    this.isLive = e.data && e.data.type === 'dynamic'
  },

  /** Listener for 'play' event. */
  playbackStarted: function (e) {
    this.fireStart({}, 'playbackStarted')
  },

  /** Listener for 'pause' event. */
  playbackPaused: function (e) {
    this.firePause({}, 'playbackPaused')
  },

  /** Listener for 'playing' event. */
  playbackPlaying: function (e) {
    if (this.flags.isPaused) {
      this.fireResume({}, 'playbackPlaying')
    }
    if (!this.player.isPaused() && this.flags.isSeeking) {
      this.fireSeekEnd({}, 'playbackPlaying')
    }
    this.fireBufferEnd({}, 'playbackPlaying')
    this.fireJoin({}, 'playbackPlaying')
  },

  playbackWaiting: function () {
    this.fireBufferBegin({}, false, 'playbackWaiting')
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    if (e.error.code) {
      this.fireError(e.error.code, e.error.message, undefined, undefined, 'errorListener')
    } else {
      this.fireError(e.error, 'Error', undefined, undefined, 'errorListener')
    }
  },

  /** Listener for 'error' event. */
  playbackError: function (e) {
    if (e.error.code) {
      this.fireError(e.error.code, e.error.message, undefined, undefined, 'playbackError')
    } else {
      this.fireError(e.error, 'Playback error', undefined, undefined, 'playbackError')
    }
  },

  /** Listener for 'seeking' event. */
  playbackSeeking: function (e) {
    if (this.isLive){
      this.fireStart({}, 'playbackSeeking')
    }
    this.fireSeekBegin({}, false, 'playbackSeeking')
  },

  /** Listener for 'ended' event. */
  playbackEnded: function (e) {
    this.fireStop({}, 'playbackEnded')
  },

  getCdnTraffic: function () {
    if (this.player.getMetricsFor) {
      var allMetrics = this.player.getMetricsFor('p2pweb')
      if (!allMetrics) return youbora.Adapter.prototype.getCdnTraffic()
      var metrics = allMetrics.metricsP2PWeb
      return metrics.videoAvgLength * metrics.chunksFromCDN +
        metrics.audioAvgLength * metrics.chunksFromCDN
    }
    return null
  },

  getP2PTraffic: function () {
    if (this.player.getMetricsFor) {
      var allMetrics = this.player.getMetricsFor('p2pweb')
      if (!allMetrics) return youbora.Adapter.prototype.getP2PTraffic()
      var metrics = allMetrics.metricsP2PWeb
      return metrics.videoAvgLength * metrics.chunksFromP2P +
        metrics.audioAvgLength * metrics.chunksFromP2P
    }
    return null
  }
})

module.exports = youbora.adapters.DashJS
